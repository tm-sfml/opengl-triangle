#pragma once
#include <Impl/ObjectBuffers.h>

float PlaneVertices[] 
{
        // positions            // normals         // texcoords
         25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,
        -25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,   0.0f,  0.0f,
        -25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,

         25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,
        -25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,
         25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,  25.0f, 10.0f
};

std::vector<int> PlaneVertexAttributeSizes {3, 3, 2};

ObjectBuffers CreatePlaneBuffers()
{
	return {{PlaneVertices, sizeof PlaneVertices}, PlaneVertexAttributeSizes};
}
