#pragma once
#include <Impl/FrameBuffer.h>
#include <Impl/Shader.h>
#include <glm/glm.hpp>
#include <functional>
#include "Shaders/shader_consts.h"

// calculates the depth buffer of the scene as texture,
// used for shadows implementation
class ShadowMap
{
public:
	ShadowMap(unsigned width, unsigned height)
		: m_vertOnlyShader("Cubes/Shaders/Vert/vert_lighting.vert", "Cubes/Shaders/Frag/single_color.frag")
	{
		m_depthTextureBuffer.CreateDepthTexture(width, height);

		m_depthTextureBuffer.Bind();
		glDrawBuffer(GL_NONE);
		glReadBuffer(GL_NONE);

		if(!FrameBuffer::IsBoundFramebufferComplete())
		{
			std::cout << "ERROR::FRAMEBUFFER:: Framebuffer is not complete!" << std::endl;
		}
		FrameBuffer::BindDefault();
	}

	// render depth of scene to texture (from light's perspective)
	void CalculateDepthBufferTexture(glm::mat4 const& lightSpaceTransform , glm::vec3 lightPos, std::function<void(Shader const&)> m_sceneRenderer)
	{
        m_vertOnlyShader.use();
		m_vertOnlyShader.setMat4(ShaderConsts::Vert::Lighting::ModelToProj, lightSpaceTransform);
		m_vertOnlyShader.setVec3(ShaderConsts::Vert::Lighting::ViewPos, GlobalCamera.Position);
		m_vertOnlyShader.setVec3(ShaderConsts::Frag::Lighting::LightPos, lightPos);

		m_depthTextureBuffer.Bind();
		FrameBuffer::ClearBoundFrameBuffer();

		m_sceneRenderer(m_vertOnlyShader);
		FrameBuffer::BindDefault(); 
	}

	unsigned Width() { return GetDepthTexture().GetWidth(); }
	unsigned Height() { return GetDepthTexture().GetHeight(); }

	Texture const& GetDepthTexture() const { return m_depthTextureBuffer.GetDepthTexture().value(); }

private:
	FrameBuffer m_depthTextureBuffer;
	Shader m_vertOnlyShader;
};
