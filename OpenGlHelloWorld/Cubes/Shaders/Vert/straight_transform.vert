#version 330 core
// straight from local to projection

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;

uniform mat4 localToProjection;

void main()
{
    gl_Position = localToProjection * vec4(aPos, 1.0f);
}