#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
// layout (location = 1) in vec2 aTexCoord;

// out vec2 TexCoord;
// in world coords
out vec3 FragPos;
out vec3 Normal;
out vec4 FragPosLightSpace;


uniform mat4 modelToProjection;
uniform mat4 model;
uniform mat4 lightSpaceMatrix;

void main()
{
    gl_Position = modelToProjection * model * vec4(aPos, 1.0f);
	mat3 normalMat = mat3(transpose(inverse(model)));
	Normal = normalMat * aNormal;
	FragPos = vec3(model * vec4(aPos, 1.0));

	FragPosLightSpace = lightSpaceMatrix * vec4(FragPos, 1.0);
   // TexCoord = vec2(aTexCoord.x, 1.0 - aTexCoord.y);
}