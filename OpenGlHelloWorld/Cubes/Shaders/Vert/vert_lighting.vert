#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
// layout (location = 1) in vec2 aTexCoord;

// out vec2 TexCoord;
// in world coords
out vec3 FragPos;
out vec3 Normal;

uniform mat4 modelToProjection;
uniform mat4 model;

void main()
{
    gl_Position = modelToProjection * model * vec4(aPos, 1.0f);
	mat3 normalMat = mat3(transpose(inverse(model)));
	Normal = normalMat * aNormal;
	FragPos = vec3(model * vec4(aPos, 1.0));
   // TexCoord = vec2(aTexCoord.x, 1.0 - aTexCoord.y);
}