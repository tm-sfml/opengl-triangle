#pragma once

namespace ShaderConsts
{
	using ConstsCStr = char const* const;
	namespace Vert
	{
		namespace Lighting
		{
			 ConstsCStr Model = "model";
			 ConstsCStr ViewPos = "viewPos";
			 ConstsCStr ModelToProj = "modelToProjection";
			 ConstsCStr LightSpaceTransform = "lightSpaceMatrix";
		}

		namespace Lines
		{
			ConstsCStr LocalToProj = "localToProjection";
		}
	}

	namespace Frag
	{
		ConstsCStr Color = "color";

		namespace Lighting
		{
			ConstsCStr ObjectColor = "objectColor";
			ConstsCStr LightPos = "lightPos";
		}
	}
}
