#version 330 core
out vec4 FragColor;

// must be normalized
in vec3 Normal;
// in woorld coords
in vec3 FragPos; 
// 
in vec4 FragPosLightSpace;


uniform vec3 lightColor;
uniform vec3 objectColor;
uniform vec3 lightPos; 
uniform vec3 viewPos;
uniform sampler2D shadowMap;


float ShadowCalculation(vec4 fragPosLightSpace)
{
    // perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
	// NDC (0.5) to depth (1)
	projCoords = projCoords * 0.5 + 0.5; 

	float closestDepth = texture(shadowMap, projCoords.xy).r; 
	// get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;
	
	float shadow = currentDepth > closestDepth  ? 1.0 : 0.0;
	return shadow;
}


void main()
{
    // ambient
    float ambientStrength = 0.2;
    vec3 ambient = ambientStrength * lightColor;

	// diffuse
	vec3 lightDir = normalize(lightPos - FragPos);
	float diffuseStrength = max(dot(Normal, lightDir), 0.0);
	vec3 diffuseColor = diffuseStrength * lightColor;

	// specular
	float specularStrength = 0.5;
	vec3 viewDir = normalize(viewPos - FragPos);
	vec3 lightDirToFrag = -lightDir;
	vec3 reflectDir = reflect(lightDirToFrag, Normal);

	float viewToReflectCloseness = max(dot(viewDir, reflectDir), 0.0);
	float shininess = 128.0;
	vec3 specularColor = specularStrength * pow(viewToReflectCloseness, shininess) * lightColor;

	// shadow
	float shadow = ShadowCalculation(FragPosLightSpace);       

	vec3 lightingColor = ambient + (1.0 - shadow) * (diffuseColor + specularColor);
    vec3 result = lightingColor * objectColor;
    FragColor = vec4(result, 1.0);
} 