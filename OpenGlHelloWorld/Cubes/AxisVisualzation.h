#pragma once
#include "Impl/ObjectBuffers.h"
#include <glm/glm.hpp>
#include <Impl/Shader.h>
#include "cubes.h"
#include <Impl/Camera.h>
#include <Impl/mat_utils.h>
#include <Cubes/Shaders/shader_consts.h>


float const MarkerScale = 0.1f;
float const AxisTranslation = 3.f;

namespace Uniforms = ShaderConsts::Vert::Lighting;

std::vector<glm::vec3> MarkedPoints
{
	Vectors::Zero, UnitVectors::X, UnitVectors::Y, UnitVectors::Z, 
};

std::vector<glm::vec3> MarkerColors
{
	{1.f, 1.f, 1.f}
	, {1.f, 0.f, 0.f}
	, {0.f, 1.f, 0.f}
	, {0.f, 0.f, 1.f}
};



class AxisVisualization
{
public:
	AxisVisualization()
		: m_cubeBuffers(CreateCubeBuffers())
		, m_shader("Cubes/Shaders/Vert/vert_lighting.vert", "Cubes/Shaders/Frag/single_color.frag")
	{
		for (auto& marker : MarkedPoints)
		{
			marker *= AxisTranslation;
		}

		for (auto marker : MarkedPoints)
		{
			float const points[]
			{
				0.f, 0.f, 0.f,
				marker.x, marker.y, marker.z 
			};
			ObjectBuffers pointVertices({points, sizeof(points)}, {3});
			m_axisLinesVertexArrays.push_back(pointVertices);
		}
	}

	void Render(glm::mat4 const& modelToProj)
	{
		m_shader.use();
		m_shader.setMat4(Uniforms::ModelToProj, modelToProj);

		for (size_t i = 0; i < MarkedPoints.size(); i++)
		{
			auto marker = MarkedPoints.at(i);
			auto color = MarkerColors.at(i);
			auto lineVertices = m_axisLinesVertexArrays.at(i);

			DrawMarker(marker, color);

			// -------------
			// draw axis lines
			// we must clear the cube's model transform as (0, 0, 0) is already in woorld coordinates
			m_shader.use();
			m_shader.setMat4(Uniforms::Model, glm::mat4(1.0f));
			m_shader.setVec3(ShaderConsts::Frag::Color, color);
			lineVertices.Bind();
			glDrawArrays(GL_LINES, 0, 2);
		}
	}

	// used for shadowmapping
	//void RenderWithCustomShader()

private:

	Shader m_shader;
	ObjectBuffers m_cubeBuffers;

	std::vector<ObjectBuffers> m_axisLinesVertexArrays;

	// axis is zero for origin
	void DrawMarker(glm::vec3 position, glm::vec3 color)
	{
		auto markerModel = glm::translate(glm::mat4(1.f), position);
		markerModel = glm::scale(markerModel, glm::vec3(MarkerScale));

		m_shader.use();
		m_shader.setMat4(Uniforms::Model, markerModel);
		m_shader.setVec3(ShaderConsts::Frag::Color, color);

		m_cubeBuffers.Bind();
		glDrawArrays(GL_TRIANGLES, 0, 36);
	}
};
