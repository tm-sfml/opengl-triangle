#pragma once
#include <Impl/Shader.h>
#include <Impl/ObjectBuffers.h>
#include <Impl/Program.h>
#include <Impl/Texture.h>
#include "ShadowMap.h"
#include <glm/glm.hpp>
#include "cubes.h"
#include "plane.h"
#include "AxisVisualzation.h"
#include <Cubes/Shaders/shader_consts.h>
#include <Impl/TextureVisualizer.h>

glm::vec3 const LightPosition(2.f, 6.0f, 5.f);
glm::vec3 const CubePosition(1.f, 3.f, 1.f);

Color LightColor = Color::FromInts(253, 255, 181);
Color CubeColor = Color::FromInts(48, 135, 50);
Color PlaneColor = Color::FromInts(58, 183, 146);

namespace NamesVert = ShaderConsts::Vert::Lighting;
namespace NamesFrag = ShaderConsts::Frag::Lighting;

unsigned const ShadowWidth = 1024;
unsigned const ShadowHeight = 1024;

class CubesProgram : public Program
{
public:
	CubesProgram()
		: m_shader("Cubes/Shaders/Vert/shadow_lighting.vert", "Cubes/Shaders/Frag/color_lighting.frag")
		, m_lightShader("Cubes/Shaders/Vert/shadow_lighting.vert", "Cubes/Shaders/Frag/single_color.frag")
		, m_cubesBuffers(CreateCubeBuffers())
		, m_planeVertices(CreatePlaneBuffers())
		, m_lightPos(LightPosition)
		, m_shadowMap(m_window.Width, m_window.Height)
	{
		m_lightShader.setVec3("color", LightColor.ToVector());
		m_shader.setVec3("lightColor", LightColor.ToVector());
		m_shader.setInt("shadowMap", 0);
	}

protected:
	void OnRender() override
	{
		// move the light around the cube
		m_lightPos.x = 1.0f + sin(glfwGetTime()) * 2.0f;
        m_lightPos.z = sin(glfwGetTime() / 2.0f) * 1.0f;

		auto lightSpaceTransform = CalculateLightSpaceMatrix();

		m_shadowMap.CalculateDepthBufferTexture(lightSpaceTransform, m_lightPos, 
			[this](auto const& shader) { RenderScene(shader); }  );

		FrameBuffer::ClearBoundFrameBuffer();

		//---------------
		//camera projection
		glm::mat4 const view = GlobalCamera.GetViewMatrix();
		float const aspect = m_window.Width / m_window.Height;
        glm::mat4 const projection = glm::perspective(glm::radians(GlobalCamera.Zoom), aspect, 0.1f, 100.0f);
		glm::mat4 const modelToProjection = projection * view;

		m_shader.setMat4(NamesVert::ModelToProj, modelToProjection);
		m_shader.setMat4(NamesVert::LightSpaceTransform, lightSpaceTransform);
		m_shader.setVec3(NamesVert::ViewPos, GlobalCamera.Position);
		m_shader.setVec3(NamesFrag::LightPos, m_lightPos);
		
		m_shadowMap.GetDepthTexture().Bind();

		//RenderScene(m_shader);
		//RenderLight(modelToProjection);
		//m_axis.Render(modelToProjection);

		m_textureVisualizer.DrawTexture(m_shadowMap.GetDepthTexture());
	}

private:
	glm::vec3 m_lightPos;
	ObjectBuffers m_cubesBuffers;
	ObjectBuffers m_planeVertices;

	Shader m_shader;
	Shader m_lightShader;

	AxisVisualization m_axis;
	ShadowMap m_shadowMap;
	TextureVisualizer m_textureVisualizer;


	// shaders differ when we render to construct shadowmap and to actually draw the scene
	// only "model" and "objectColor" uniforms of shader are set, any other global-for-each-scene-object uniform must be set beforehand
	void RenderScene(Shader const& shader)
	{
		//---------
		// cube
		auto model = glm::translate(glm::mat4(1.0f), CubePosition);
		shader.setMat4(NamesVert::Model, model);
		shader.setVec3(NamesFrag::ObjectColor, CubeColor.ToVector());

		shader.use();
        m_cubesBuffers.Bind();
		glDrawArrays(GL_TRIANGLES, 0, 36);

		//----------------------------
		// plane
		shader.setMat4(NamesVert::Model, glm::mat4(1.0f));
		shader.setVec3(NamesFrag::ObjectColor, PlaneColor.ToVector());

		shader.use();
		m_planeVertices.Bind();
		glDrawArrays(GL_TRIANGLES, 0, 6);
	}

	void RenderLight(glm::mat4 const& modelToProj)
	{
		//---------------
		// render light
		auto lightModel = glm::translate(glm::mat4(1.0f), m_lightPos);
		float const lightScale = 0.2f;
		lightModel = glm::scale(lightModel, glm::vec3(lightScale));

		m_lightShader.use();
		m_lightShader.setMat4(NamesVert::ModelToProj, modelToProj);
		m_lightShader.setMat4(NamesVert::Model, lightModel);
        m_cubesBuffers.Bind();
 		glDrawArrays(GL_TRIANGLES, 0, 36);
	}

	glm::mat4 CalculateLightSpaceMatrix()
	{
		glm::mat4 lightProjection, lightView;
        glm::mat4 lightSpaceMatrix;
        float near_plane = 0.1f, far_plane = 70.5f;
        lightProjection = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, near_plane, far_plane);
        lightView = glm::lookAt(m_lightPos, glm::vec3(0.0f), glm::vec3(0.0, 1.0, 0.0));
        lightSpaceMatrix = lightProjection * lightView;

		return lightSpaceMatrix;
	}
};


