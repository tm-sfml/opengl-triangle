#pragma once
#include "Texture.h"
#include "Shader.h"

char const * FrameBuffVertShader = 
R"(
	#version 330 core
	layout (location = 0) in vec2 aPos;
	layout (location = 1) in vec2 aTexCoords;

	out vec2 TexCoords;

	void main()
	{
		TexCoords = aTexCoords;
		gl_Position = vec4(aPos.x, aPos.y, 0.0, 1.0); 
	} 
)";

char const * FrameBuffFragShader = 
R"(
	#version 330 core
	out vec4 FragColor;
  
	in vec2 TexCoords;

	uniform sampler2D depthMap;

	void main()
	{             
		vec4 t = texture(depthMap, TexCoords);
		float depthValue = t.r;
		float offset = 0.5 - depthValue;
		// depthValue += offset;

		FragColor = vec4(vec3(depthValue), 1.0);
	}

)";


class TextureVisualizer
{
public:
	TextureVisualizer()
		: m_frameBufferDisplayShader(Shader::FromSourceCode(FrameBuffVertShader, FrameBuffFragShader))
	{}

	void DrawTexture(Texture const& texture)
	{
		m_frameBufferDisplayShader.use();  
		texture.Bind();
		RenderQuad();
	}

private:
	Shader m_frameBufferDisplayShader;

		// -----------------------------------------
	// renders a 1x1 XY quad in NDC
	void RenderQuad()
	{
		unsigned quadVAO = 0;
		unsigned quadVBO;

		if (quadVAO == 0)
		{
			float quadVertices[] = {
				// positions        // texture Coords
				-1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
				-1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
				 1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
				 1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
			};
			// setup plane VAO
			glGenVertexArrays(1, &quadVAO);
			glGenBuffers(1, &quadVBO);
			glBindVertexArray(quadVAO);
			glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
			glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
			glEnableVertexAttribArray(1);
			glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
		}
		glBindVertexArray(quadVAO);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		glBindVertexArray(0);
	}
};
