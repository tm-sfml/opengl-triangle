#pragma once

struct VoidArrayData
{
	void const* Data = nullptr;
	int Size = 0;
};
