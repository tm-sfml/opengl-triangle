#pragma once
#include "Color.h"
#include "window.h"
#include "consts.h"
#include "Camera.h"
#include <functional>
#include "debug.h"

// global - hate C and opengl
Camera GlobalCamera(glm::vec3(0.f, 5.0f, 8.0f));

float lastX;
float lastY;
bool firstMouse = true;


// timing
float deltaTime = 0.0f;	// time between current frame and last frame
float lastFrame = 0.0f;

// originally i had a couple of programs - my labs for university course,
class Program
{
public:
	Program(float windowWidth = Consts::Width, float windowHeight = Consts::Height)
		: m_window(windowWidth, windowHeight)
		, m_backgroundColor(Consts::DefaultBackground)
	{
		lastX = m_window.Width / 2.0f;	
		lastY = m_window.Height / 2.0f;
		glEnable(GL_DEPTH_TEST);

		glfwSetCursorPosCallback(m_window.Pointer, mouse_callback);
		glfwSetScrollCallback(m_window.Pointer, scroll_callback);
	}

	~Program()
	{
		glfwTerminate();
	}

	void SetBackgroundColor(Color color) { m_backgroundColor = color; }

	void Render()
	{
		while(!glfwWindowShouldClose(m_window.Pointer))
		{
			ProcessInput(m_window.Pointer);

			float currentFrame = glfwGetTime();
			deltaTime = currentFrame - lastFrame;
			lastFrame = currentFrame;

			auto const c = m_backgroundColor;
			glClearColor(c.Red, c.Green, c.Blue, c.Alpha);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			OnRender();

			glfwSwapBuffers(m_window.Pointer);
			glfwPollEvents();   
		}
	}

protected:
	// to clear buffer with
	Color m_backgroundColor;
	Window m_window;

	// runs every iteration of render loop
	virtual void OnRender() = 0;

private:
	static void framebuffer_size_callback(GLFWwindow* window, int width, int height)
	{
		// make sure the viewport matches the new window dimensions; note that width and 
		// height will be significantly larger than specified on retina displays.
		glViewport(0, 0, width, height);
	}

	static void mouse_callback(GLFWwindow* window, double xpos, double ypos)
	{
		if (firstMouse)
		{
			lastX = xpos;
			lastY = ypos;
			firstMouse = false;
		}

		float xoffset = xpos - lastX;
		float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

		lastX = xpos;
		lastY = ypos;

		GlobalCamera.ProcessMouseMovement(xoffset, yoffset);
	}

	static void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
	{
		GlobalCamera.ProcessMouseScroll(yoffset);
	}

	void ProcessInput(GLFWwindow *window)
	{
		if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
			glfwSetWindowShouldClose(window, true);

		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
			GlobalCamera.ProcessKeyboard(FORWARD, deltaTime);
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
			GlobalCamera.ProcessKeyboard(BACKWARD, deltaTime);
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
			GlobalCamera.ProcessKeyboard(LEFT, deltaTime);
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
			GlobalCamera.ProcessKeyboard(RIGHT, deltaTime);
	}
};
