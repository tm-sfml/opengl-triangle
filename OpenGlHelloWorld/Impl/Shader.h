#pragma once

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

class Shader
{
public:
    unsigned int ID;

	static Shader FromSourceCode(std::string const& vertSourceCode, std::string const& fragSourceCode)
	{
		Shader shader;
		shader.compile(vertSourceCode, fragSourceCode);
		return shader;
	}

    Shader(std::string const& vertexPath, std::string const& fragmentPath)
    {
		compile(readFull(vertexPath), readFull(fragmentPath));
    }

    // activate the shader
    // ------------------------------------------------------------------------
    void use() const
    { 
        glUseProgram(ID); 
    }
    // utility uniform functions
    // ------------------------------------------------------------------------
    void setBool(const std::string &name, bool value) const
    {         
		use();
        glUniform1i(glGetUniformLocation(ID, name.c_str()), (int)value); 
    }
    // ------------------------------------------------------------------------
    void setInt(const std::string &name, int value) const
    { 
		use();
        glUniform1i(glGetUniformLocation(ID, name.c_str()), value); 
    }
    // ------------------------------------------------------------------------
    void setFloat(const std::string &name, float value) const
    { 
		use();
        glUniform1f(getUniformLocation(name), value); 
    }

	void setMat4(const std::string &name, glm::mat4 const& value) const
    {
		use();
	    glUniformMatrix4fv(getUniformLocation(name), 1, GL_FALSE, glm::value_ptr(value));
    }

	void setVec3(const std::string &name, glm::vec3 vec) const
	{
		use();
		glUniform3f(getUniformLocation(name), vec.x, vec.y, vec.z);
	}

private:
	Shader() = default;

    // utility function for checking shader compilation/linking errors.
    // ------------------------------------------------------------------------
    void checkCompileErrors(unsigned int shader, std::string type)
    {
        int success;
        char infoLog[1024];
        if (type != "PROGRAM")
        {
            glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
            if (!success)
            {
                glGetShaderInfoLog(shader, 1024, NULL, infoLog);
                std::cout << "ERROR::SHADER_COMPILATION_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
            }
        }
        else
        {
            glGetProgramiv(shader, GL_LINK_STATUS, &success);
            if (!success)
            {
                glGetProgramInfoLog(shader, 1024, NULL, infoLog);
                std::cout << "ERROR::PROGRAM_LINKING_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
            }
        }
    }

	int getUniformLocation(const std::string &name) const
    {
	    return glGetUniformLocation(ID, name.c_str());
    }

	static std::string readFull(std::string const& filename)
	{
		std::ifstream file;
        file.exceptions (std::ifstream::failbit | std::ifstream::badbit);
		try
		{
			file.open(filename);
			std::stringstream stream;
			stream << file.rdbuf();
			file.close();
			return stream.str();
		}
		catch (const std::exception&)
		{
            std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ : " << filename << std::endl;
			return "";
		}
	}

	void compile(std::string const& vertSourceCode, std::string const& fragSourceCode)
	{
		auto vertexCodeCStr = vertSourceCode.c_str();
		auto fragmentCodeCStr = fragSourceCode.c_str();

		// 2. compile shaders
        unsigned int vertex, fragment;
        // vertex shader
        vertex = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vertex, 1, &vertexCodeCStr, NULL);
        glCompileShader(vertex);
        checkCompileErrors(vertex, "VERTEX");
        // fragment Shader
        fragment = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragment, 1, &fragmentCodeCStr, NULL);
        glCompileShader(fragment);
        checkCompileErrors(fragment, "FRAGMENT");
        // shader Program
        ID = glCreateProgram();
        glAttachShader(ID, vertex);
        glAttachShader(ID, fragment);
        glLinkProgram(ID);
        checkCompileErrors(ID, "PROGRAM");
        // delete the shaders as they're linked into our program now and no longer necessary
        glDeleteShader(vertex);
        glDeleteShader(fragment);
	}
};
