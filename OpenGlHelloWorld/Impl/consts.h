#pragma once
#include "Color.h"

namespace Consts
{
	Color const DefaultBackground = Color::FromInts(35, 31, 47, 220);
	constexpr float Width = 1600.f;
	constexpr float Height = 900.f;
}
