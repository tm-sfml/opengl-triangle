#pragma once
#include "glm/glm.hpp"

// 0 to 1
struct Color
{
	float Red = 0;
	float Green = 0;
	float Blue = 0;
	float Alpha = 1.f;

	static Color FromInts(int red, int green, int blue, int alpha = 255)
	{
		float const maxColorVal = 255.0f;
		return
		{
			static_cast<float>(red) / maxColorVal,
			static_cast<float>(green) / maxColorVal,
			static_cast<float>(blue) / maxColorVal,
			static_cast<float>(alpha) / maxColorVal
		};
	}

	glm::vec4 ToVector()
	{
		return {Red, Green, Blue, Alpha};
	}
};
