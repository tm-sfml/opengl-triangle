#pragma once
#include <glad/glad.h>
#include <vector>
#include "VoidArrayData.h"
#include <cassert>
#include "stl_utils.h"
#include <optional>

// creates and binds vertex buffer (VBO), Vertex Array(VAO) and optionally Element Buffer (EBO), 
// stores their ids and clears them
class ObjectBuffers
{
public:
	// vertex attribute locations are set consecutively starting from 0
	// indices can be nullptr
	ObjectBuffers(VoidArrayData vertices, std::vector<int> vertexAttributeSizes, VoidArrayData indices = {})
	{
	    glGenVertexArrays(1, &m_vertexArrayId);
	    glGenBuffers(1, &m_vertexBufferId);

	    glBindVertexArray(m_vertexArrayId);

	    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferId);
	    glBufferData(GL_ARRAY_BUFFER, vertices.Size, vertices.Data, GL_STATIC_DRAW);

		if (indices.Data)
		{
			m_indicesArrayId = 0;
		    glGenBuffers(1, &m_indicesArrayId.value());
		    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *m_indicesArrayId);
		    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.Size, indices.Data, GL_STATIC_DRAW);
		}

		 //setup attribute layout
		int const stride = Sum(vertexAttributeSizes) * sizeof(float);
		int attributeIndex = 0;
		int attributeArrayIndex = 0;
		for (int attributeSize : vertexAttributeSizes)
		{
			assert(attributeArrayIndex <= stride);
			// TODO: different attribute types
			glVertexAttribPointer(attributeIndex, attributeSize, GL_FLOAT, GL_FALSE, stride, (void*)attributeArrayIndex);
			glEnableVertexAttribArray(attributeIndex);
			attributeIndex++;
			attributeArrayIndex += attributeSize * sizeof(float);
		}
	}

	unsigned GetVertexArrayId() const { return m_vertexArrayId; }

	void Bind() { glBindVertexArray(GetVertexArrayId()); }

private:
	unsigned m_vertexArrayId;
	unsigned m_vertexBufferId;
	std::optional<unsigned> m_indicesArrayId;
};
