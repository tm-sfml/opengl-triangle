#pragma once
#include <glad/glad.h>
#include <glm/glm.hpp>

namespace Vectors
{
	glm::vec3 Zero(0.f);
}

namespace UnitVectors
{
	glm::vec3 const X(1.0f, 0.0f, 0.0f);
	glm::vec3 const Y(0.0f, 1.0f, 0.0f);
	glm::vec3 const Z(0.0f, 0.0f, 1.0f);
};

