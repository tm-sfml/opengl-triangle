#pragma once
#include <string>
#include <External/stb_image.h>
#include <iostream>
#include <glad/glad.h>


class Texture
{
public:
	// empty texture
	Texture(unsigned width, unsigned height, GLint format, GLint dataType)
	{
		Create();
		Bind();
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, dataType, NULL);
		
		// TODO: try without
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	}

	// loads texture from file
	Texture(std::string const& filename)
	{
		Create();

		int width, height, channelsCount;
		// load and generate the texture
		unsigned char *data = stbi_load(filename.c_str(), &width, &height, &channelsCount, 0);
		if (data)
		{
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
			glGenerateMipmap(GL_TEXTURE_2D);
		}
		else
		{
		    std::cout << "Failed to load texture" << std::endl;
		}
		stbi_image_free(data);
	}

	unsigned GetId() const { return m_id; }
	unsigned GetWidth() const { return m_width; }
	unsigned GetHeight() const { return m_height; }

	void Bind() const
	{
		glBindTexture(GL_TEXTURE_2D, m_id);
	}

private:
	unsigned m_id;
	unsigned m_width;
	unsigned m_height;

	// generates empty opengl texture with default settings and stores reference to it in m_id
	void Create()
	{
		int const texCount = 1;
		glGenTextures(texCount, &m_id);
		Bind();

		// set the texture wrapping/filtering options (on the currently bound texture object)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}
};
