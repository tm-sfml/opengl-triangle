#pragma once
#include "Texture.h"
#include <optional>
#include <Impl/consts.h>

class FrameBuffer
{
public:
	// also binds the buffer
	static bool IsBoundFramebufferComplete()
	{
		return glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE; 
	}

	static void ClearBoundFrameBuffer(GLint bits = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
	{
		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		glClear(bits);
	}

	FrameBuffer()
	{
		glGenFramebuffers(1, &m_id);
	}

	void CreateDepthTexture(unsigned width, unsigned height)
	{
		assert(!m_depthBufferTexture);

		m_depthBufferTexture.emplace(width, height, GL_DEPTH_COMPONENT, GL_FLOAT);
		Bind();
		m_depthBufferTexture->Bind();
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_depthBufferTexture->GetId(), 0);
	}

	void CreateColorTexture(unsigned width, unsigned height)
	{
		assert(!m_colorBufferTexture);

		m_colorBufferTexture.emplace(width, height, GL_RGB, GL_UNSIGNED_BYTE);
		Bind();
		m_colorBufferTexture->Bind();

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_colorBufferTexture->GetId(), 0);
	}

	unsigned GetId() const { return m_id; }
	std::optional<Texture> const& GetDepthTexture() const { return m_depthBufferTexture; }
	std::optional<Texture> const& GetColorTexture() const { return m_colorBufferTexture; }

	void Bind() const { glBindFramebuffer(GL_FRAMEBUFFER, m_id); }
	static void BindDefault() { glBindFramebuffer(GL_FRAMEBUFFER, 0); }

private:
	unsigned m_id;
	std::optional<Texture> m_depthBufferTexture;
	std::optional<Texture> m_colorBufferTexture;
};