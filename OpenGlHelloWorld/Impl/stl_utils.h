#pragma once
#include <numeric>

template <class TContainer>
auto Sum(TContainer const& container)
{
	constexpr auto start = std::is_arithmetic_v<typename TContainer::value_type> ? 0 : TContainer::value_type();
	return std::accumulate(container.cbegin(), container.cend(), start);
}
