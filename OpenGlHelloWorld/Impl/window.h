#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <ostream>
#include <iostream>
#include "debug.h"

class Window
{
public:
	// captures mouse
	Window(float width, float height)
		: Width(width)
		, Height(height)
	{
		glfwInit();

	    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
	    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);

		Pointer = glfwCreateWindow(width, height, "LearnOpenGL", NULL, NULL);
		if (Pointer == nullptr)
		{
		    std::cout << "Failed to create GLFW window" << std::endl;
		    glfwTerminate();
		    return;
		}
		glfwMakeContextCurrent(Pointer);

		if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
		{
		    std::cout << "Failed to initialize GLAD" << std::endl;
		    return;
		}	

		// set window size
		glViewport(0, 0, width, height);
		glfwSetFramebufferSizeCallback(Pointer, [](GLFWwindow*, int width, int height)
		{
		    glViewport(0, 0, width, height);
		} );

		// tell GLFW to capture our mouse
		glfwSetInputMode(Pointer, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

		SetupDebug();
	}

	GLFWwindow* Pointer;
	float const Width;
	float const Height;
};
