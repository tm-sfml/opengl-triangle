#pragma once
#include "Impl/Shader.h"
#include "Impl/ObjectBuffers.h"

float const Vertices[]
{
    -0.5f, -0.5f, 0.0f,
     0.5f, -0.5f, 0.0f,
     0.0f,  0.5f, 0.0f
};

static Shader CreateTriangleShaderProgram()
{
	return Shader("Triangle/vert.vert", "Triangle/frag.frag");
}

static ObjectBuffers CreateTriangleVao()
{
	return ObjectBuffers({ Vertices, sizeof(Vertices) }, {3});
}
