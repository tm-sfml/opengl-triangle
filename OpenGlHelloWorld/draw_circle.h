#pragma once
#include <stdio.h>
#include <math.h>
#include <glad/glad.h>
#include <Impl/Program.h>
#include<stdio.h>  

void myinit(void)
{
	glClearColor(1.0,1.0,1.0,0.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity(); // multiply the current matrix by identity matrix
 }
 
float xstart=10 ,ystart=100,xend=400,yend=10,x,y;

int Round(float a)//any x i.e 1>=a>=0.5 is rounded to 1
{
  if(a-int(a)>=0.5)
  return int(a)+1;
  else
  return int(a);
}

void bressen()
     {   
       float ydiff = yend-ystart;
       float xdiff = xend-xstart;  
       
        float p0;
         p0 = 2*(ydiff)-xdiff; 
           
        x = xstart;               //assign xstart to x
        y = ystart;              //assign ystart to y

while ((x != xend+1)&&(y != yend+1))
      {   
       glBegin(GL_POINTS); // writes pixels on the frame buffer with the current drawing color
       glVertex2i(Round(x),Round(y));//sets vertex
	   glEnd();
if (p0<0)   
       {
		x = x+1;
		y = y;
		p0=p0+2*(ydiff);
       }
       else
       {
         x=x+1;
         y=y+1;
         p0=2*(ydiff)-2*(xdiff);
       }
 
}
}


class CircleProgram : public Program
{
public:
	CircleProgram()
	{

	}

	void OnRender() override
	{
		glClearColor(0.4,0.7,0.2,1.0);  
		glColor3f(0.5,0.3,0.0);  
		glClear(GL_COLOR_BUFFER_BIT);
		

		bressen();  

		glFlush();  
	};
};
